from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from users.models import profile

# from crispy_forms.helper import FormHelper
class  UserRegistrationForm(UserCreationForm):
	# helper=FormHelper()
	# helper.form_show_helper=False   
	email=forms.EmailField()
	# gender=forms.CharField(max_length=10)

	# Last_name=forms.CharField()
	class Meta:
		model=User
		fields=['first_name','last_name','username','email','password1','password2']

class UserUpdateForm(forms.ModelForm):
	email=forms.EmailField()
	class Meta:
		model=User
		fields=['username','email']
class ProfileUpdateForm(forms.ModelForm):
	class Meta:
		model=profile
		fields=['image']