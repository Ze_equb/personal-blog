from django.db import models
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.db.models.signals import post_save
from PIL import Image
class User(AbstractBaseUser):

class profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} profile'
    # def save(self):
    # 	super().save()
    # 	img=Image.open(self.image.path)
    # 	if img.height >300 and img.width> 300:
    # 		output_size=(300,300)
    # 		img.thumbnail(output_size)
    # 		img.save(self.image.path)
    def save(self, *args, **kwargs):
        super(profile, self).save(*args, **kwargs)
        img = Image.open(self.image.path)
        if img.height > 300 or img.width > 300:
            output_size = (300,300)
            img.thumbnail(output_size)
            img.save(self.image.path)
def create_profile(sender,**kwargs):# as soon as a user is created make profile corresponding to him
    if kwargs['created']:
        profile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile,sender=User)




