-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2019 at 01:22 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecommerce2`
--

-- --------------------------------------------------------

--
-- Table structure for table `products_category`
--

CREATE TABLE IF NOT EXISTS `products_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `category_type` varchar(120) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `products_category`
--

INSERT INTO `products_category` (`id`, `category_name`, `category_type`, `amount`) VALUES
(2, 'Fruits and Vegetables', 'Food', 3),
(3, 'Meat and Fish', 'Food', 2),
(4, 'Beverages', 'Drink', 1),
(5, 'Home and cleaning', 'Neatness', 1),
(6, 'Office products', 'Materials', 1),
(7, 'Beauty Products', 'Materials', 3),
(8, 'Health Products', 'Medicine', 2),
(9, 'Pest Control', 'Car Material', 2),
(10, 'Home Appliances', 'Materials', 2),
(11, 'Shoes', 'Clothes', 3),
(12, 'Jackets', 'Clothes', 3),
(13, 'Trousers', 'Clothes', 3),
(14, 'T-shirt', 'Clothes', 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
